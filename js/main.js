let burger = document.querySelector('#bur');
let sign_in = document.querySelector('#sign_in');
let mob_sign_in = document.querySelector('#mob_sign_in');
let sidebar = document.querySelector('.sidebar');
let button_block = document.querySelector('.button-block');
let modal = document.querySelector('.modal');
let block_overlay = document.querySelector('.block_overlay');
let close_modal = document.querySelector('.close_modal');
let closeCookie = document.querySelector('.close_cookie');

function classToggle() {
  sidebar.classList.toggle('open');
}
burger.addEventListener('click', classToggle);

sign_in.addEventListener('click', function () {
  modal.classList.add('open');
  block_overlay.classList.add('open');
});

mob_sign_in.addEventListener('click', function () {
  modal.classList.add('open');
  block_overlay.classList.add('open');
});

close_modal.addEventListener('click', function () {
  modal.classList.remove('open');
  block_overlay.classList.remove('open');
});
block_overlay.addEventListener('click', function () {
  modal.classList.remove('open');
  block_overlay.classList.remove('open');
});

button_block.addEventListener('click', function validateForm (e) {
  e.preventDefault();
  
  let password = document.getElementById('password');
  let validate = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{6,}$/;
  
  if (!validate.test(password.value) ) {
      alert('Incorrect password!');
      return false;
  }
  console.log('correct pass!');
  
  let email = document.getElementById('email');
  let email_regexp = /[0-9a-zа-я_A-ZА-Я]+@[0-9a-zа-я_A-ZА-Я^.]+\.[a-zа-яА-ЯA-Z]{2,4}/i;
  
  if (!email_regexp.test(email.value)) {
      alert('Incorrect email');
      return false;
  }
  
  console.log('correct email!');
});

closeCookie.addEventListener('click', function () {
  localStorage.setItem("notificationWasShown",true);
  document.querySelector('.footer_cookie_notification').style.display = "none";
});

function NotificationCheck() {
    if (localStorage.getItem("notificationWasShown") == 'true') {
        document.querySelector('.footer_cookie_notification').style.display = "none";
    }
}
window.onload = function () {
  setTimeout(function () {
    NotificationCheck();
  }, 0);
};